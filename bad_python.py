# this is an example out of our training course: https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab6/
# it does a dumb bad thing that the SAST should definitely complain about.
import subprocess

in = input("Enter your server ip: ")
subprocess.run(["ping", in])

print("Attempting to connect to the server")
print("Application authentication was successful")